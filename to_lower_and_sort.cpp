#include "to_lower_and_sort.h"

static size_t check(char);  // check symbol in ASCII table
//static void custom_strcpy(char*, const char*, size_t, size_t);
static size_t separator_check(char);

char* get_string() {
    struct buffer {
        char* string;
        size_t size;
        size_t capacity;
    } buf = {NULL, 0, 0};

    char c = '\0';
    while (scanf("%c", &c), c != EOF && c != '\n') {
        if (buf.size + 1 >= buf.capacity) {
            size_t new_capacity = !buf.capacity ? 1 : buf.capacity * 2;
            char* tmp = (char*)malloc((new_capacity + 1) * sizeof(char));

            if (tmp == NULL) {
                if (buf.string) {
                    free(buf.string);
                }
                return NULL;
            }
            if (buf.string) {
                tmp = strcpy(tmp, buf.string);
                free(buf.string);
            }

            buf.string = tmp;
            buf.capacity = new_capacity;
        }

        buf.string[buf.size] = c;
        buf.string[buf.size + 1] = '\0';
        ++buf.size;
    }

    return buf.string;
}

void parse(char** array, const char* str) {
    if (!array) {
        printf("%s\n", strerror(EFAULT));
        return;
    }
    size_t words_count = 0;
    size_t array_iterator = 0;

    size_t i = 0;
    size_t word_len = 1;
    while (str[i] != '\0') {
        if (!separator_check(str[i]) && separator_check(str[i + 1])) {  // check last letter of word
            words_count++;

            size_t left_symbol = i;
            while (left_symbol != 0) {  // counting word length
                if (separator_check(str[left_symbol])) {
                    word_len--;
                    break;
                } else {
                    word_len++;
                    left_symbol--;
                }
            }

            array[array_iterator] = (char*)malloc((word_len + 1) * sizeof(char));  // memory for word and '\0'
            if (!left_symbol) {
                memmove(array[array_iterator], str + left_symbol, word_len);
            } else {
                memmove(array[array_iterator], str + left_symbol + 1, word_len);
            }
            array[array_iterator][word_len + 1] = '\0';

            array_iterator++;
            word_len = 1;
        }
        i++;
    }
    array[array_iterator] = (char*)malloc(sizeof(char));
    array[array_iterator][0] = '\0';
}

void custom_to_lower(char** array, char** lower_array) {
    size_t i = 0;
    while (array[i] != NULL) {
        size_t word_len = 1;
        for (size_t j = 0; array[i][j] != '\0'; j++) {
            word_len++;
        }

        lower_array[i] = (char*)malloc(word_len * sizeof(char));
        memmove(lower_array[i], array[i], word_len);
        for (size_t j = 0; array[i][j] != '\0'; j++) {
            if(check(array[i][j])) {
                custom_free(lower_array);
                return;
            }
            if (lower_array[i][j] >= UPPER_LEFT_LETTER && lower_array[i][j] <= UPPER_RIGHT_LETTER) {
                lower_array[i][j] = lower_array[i][j] + DIFFERRENCE;
            }
        }
        i++;
    }
    lower_array[i + 1] = NULL;
}

void custom_sort(char** sorted_array, char** lower_array) {
    printf("Here\n");
}

void custom_print(char** array) {
    size_t i = 0;
    while (array[i] != NULL) {
        printf("%s\n", array[i]);
        i++;
    }
}

size_t check(char c) {
    if (c == ' ') {
        return 0;
    }
    if (c < UPPER_LEFT_LETTER) {
        return 1;
    }
    if (c > UPPER_RIGHT_LETTER && c < LOWER_LEFT_LETTER && c != ' ') {
        return 1;
    }
    if (c > LOWER_RIGHT_LETTER && c != ' ') {
        return 1;
    }
    return 0;
}

size_t separator_check(char c) {
    if (c == '\0') {
        return 1;
    }
    if (c == ' ') {
        return 1;
    }
    if (c == '.') {
        return 1;
    }
    if (c == ',') {
        return 1;
    }
    return 0;
}

void custom_free(char** array) {
    size_t i = 0;
    while (array[i] != NULL) {
        i++;
    }
    for (size_t j = i - 1; j > 0; j--) {
        free(array[j]);
    }
    free(array);
}
