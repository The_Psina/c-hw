#include "to_lower_and_sort.h"

int main() {
    char* input_str = get_string();  // input and inicialize string
    if (input_str == NULL) {
        printf("%s", strerror(EFAULT));
        return EFAULT;
    }

    char** array = (char**)malloc(sizeof(char**));
    parse(array, input_str);
    if (array == NULL) {
        free(input_str);
        printf("here %s", strerror(EFAULT));
        return EFAULT;
    }/*
    printf("Whats in parsed array\n\n");
    custom_print(array);
    printf("\n\nEnd of parsed array\n");*/

    char** lower_array = NULL;
    lower_array = (char**)malloc(sizeof(char**));
    custom_to_lower(array, lower_array);
    if (lower_array == NULL) {
        free(input_str);
        custom_free(array);
        printf("%s", strerror(EFAULT));
        return EFAULT;
    }
    printf("Whats in lower array\n\n");
    custom_print(lower_array);
    printf("\n\nEnd of lower array\n");

    free(input_str);
    custom_free(array);
    custom_free(lower_array);

    return 0;
}
