#ifndef C_TO_LOWER_AND_SORT_H_
#define C_TO_LOWER_AND_SORT_H_

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<malloc.h>

#define LOWER_LEFT_LETTER 97  // a
#define LOWER_RIGHT_LETTER 122  // z

#define UPPER_LEFT_LETTER 65  // A
#define UPPER_RIGHT_LETTER 90  // Z

#define DIFFERRENCE 32


char* get_string();
void custom_free(char**);
void parse(char**,const char*);
void custom_to_lower(char**, char**);
void custom_sort(char**, char**);
void custom_print(char**);

#endif  // C_TO_LOWER_AND_SORT_H_
